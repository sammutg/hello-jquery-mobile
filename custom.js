$(document).ready(function(){
    console.log('jquery ok');
});

$(document).bind('mobileinit', function(){

    console.log('application mobile ok');
    console.log($.mobile);

    //configuration des variables par défaut
    $.mobile.pageLoadErrorMessage='Page non-trouvée !';
    $.mobile.defaultPageTransition='flip';

});

$(document).on('pageinit','#contact', function(){

    //console.log( $('#formulaire').serialize() );

    $('#validFormulaire').on('tap', function() {

        //console.log('pageinit');

        // Traitement 
        $.ajax({
            type:"GET",
            url:"form-save.php",
            data: $('#formulaire').serialize()
        }).done(function(response){
            //$.mobile.changePage( "index.html");
            alert(response);
        }).fail(function(erreur){
            alert(erreur.statusText);
            //alert('fail');
            //console.log( erreur );
        });

    });

});

$(document).on('pagebeforeshow', '#liste' ,function(){

        $.ajax({
            type:"GET",
            url:"liste-contact.html"
        }).done(function(response){
            $('#liste-contact').html(response);
            $('#liste-contact').listview('refresh');
        }).fail(function(erreur){
            alert(erreur.statusText);
        });

});

document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {
    // Now safe to use the PhoneGap API
    console.log('Device Ready');
}

// $(document).bind("pagehide", function(event, ui) {
//   $(ui.nextPage).animationComplete(function() {
//     alert('Animation completed');
//   });
// });