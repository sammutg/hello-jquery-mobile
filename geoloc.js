var map;

function initialize() {
  var optionsCarte = {
    zoom: 10,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  map = new google.maps.Map(document.getElementById('map-canvas'), optionsCarte);
}

if(navigator.geolocation) {

   function affichePosition(maPostion){
       console.log(maPostion.coords.latitude);
       
       map.setCenter(new google.maps.LatLng(maPostion.coords.latitude, maPostion.coords.longitude));
   
        var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';

        var schoolShadow = {
          url: iconBase + 'schools_maps.shadow.png',
          anchor: new google.maps.Point(16, 34)
        };

        var marker = new google.maps.Marker({
              position: new google.maps.LatLng(maPostion.coords.latitude, maPostion.coords.longitude),
              map: map,
              icon: iconBase + 'schools_maps.png',
              shadow: schoolShadow
          });

   }

   function erreurPosition(maPostion){
        console.log(error);
    }

    navigator.geolocation.getCurrentPosition(affichePosition, erreurPosition);

} else {
    alert("Ce navigateur ne supporte pas la géolocalisation");
}