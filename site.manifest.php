CACHE MANIFEST
# Version 15

CACHE:
index.html
confirm.html
custom.js
foo.html
form-save.php
form.html
grille.html
jquery-mobile.css
jquery-mobile.js
jquery.js
liste-contact.html
liste-contact.php
liste.html
styles.css

#css/themes/
css/themes/test.css
css/themes/test.min.css

#css/themes/images/
css/themes/images/ajax-loader.gif
css/themes/images/icons-18-black.png
css/themes/images/icons-18-white.png
css/themes/images/icons-36-black.png
css/themes/images/icons-36-white.png

#images/
images/ajax-loader.gif
images/icons-18-black.png
images/icons-18-white.png
images/icons-36-black.png
images/icons-36-white.png